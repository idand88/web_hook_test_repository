import os
import sys
import json

import requests
from flask import Flask, request, jsonify
import subprocess

app = Flask(__name__)


@app.route('/', methods=['GET'])
def verify():
    return "Hello world", 200


@app.route('/', methods=['POST'])
def webhook():

    # endpoint for processing incoming messaging events
    print 'Added feature 1'

    data = request.get_json()
    data = json.dumps(data)
    data.encode('utf-8')
    # Log the data
    log(data)  # you may not want to log every incoming message in production, but it's good for testing
    data = json.loads(data)

    message = {}
    if 'push' in data:
        commit_data = data['push']['changes'][0]['commits'][0]
        message['event_type'] = 'push'
        message['commit'] = {'commit_hash': commit_data['hash'], 'commit_message': commit_data['message'], 'commit_date': commit_data['date']}

    repository = data['repository']
    event_owner = repository['owner']
    message['user'] = {'username':event_owner['username'], 'display_name':event_owner['display_name'], 'uuid': event_owner['uuid']}
    message['repository'] = {'name': repository['name'], 'full_name': repository['full_name']}
    log(json.dumps(message))  # you may not want to log every incoming message in production, but it's good for testing

    print message


    return jsonify(message)


def send_message(recipient_id, message_text):

    log("sending message to {recipient}: {text}".format(recipient=recipient_id, text=message_text))

    params = {
        "access_token": os.environ["PAGE_ACCESS_TOKEN"]
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": recipient_id
        },
        "message": {
            "text": message_text
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        log(r.status_code)
        log(r.text)


def log(message):  # simple wrapper for logging to stdout on heroku
    print str(message)
    sys.stdout.flush()


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=8080,debug=True)
